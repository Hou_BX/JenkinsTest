package instrumentedclass;

import instrumentedclass.SampleClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SampleClassTest {
    private SampleClass calService;

    @Before
    public void init(){
        this.calService = new SampleClass();
    }

    @Test
    public void myAdd() {
        int result = this.calService.add(1, 2);
        Assert.assertEquals(3, result);
    }

    @Test
    public void myDivide() {
        int result = this.calService.divide(3, 2);
        Assert.assertEquals(1, result);
    }

    //@Test(expected=IllegalArgumentException.class)
    //public void myDivideException() {
    //    this.calService.divide(3, 0);
    //}
}

