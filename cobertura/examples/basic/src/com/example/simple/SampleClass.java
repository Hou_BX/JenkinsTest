package instrumentedclass;

public class SampleClass{
    public int add(int a, int b) {
        return a + b;
    }
    public int divide(int a, int b) {
        if(0 == b){
            throw new IllegalArgumentException("Test");
        }
        return a / b;
    }
}

